#version 450 core
layout(location = 0) in vec3 direction;

uniform mat4 view;
uniform mat4 projection;

out vec3 UVdir;

void main()
{
	UVdir = direction;

	mat4 rotView = mat4(mat3(view)); // remove translation from the view matrix
    gl_Position = (projection * rotView * vec4(direction, 1.0)).xyww;
}
