#version 450 core

layout(location = 0) in vec4 inPos;

uniform mat4 model;

void main()
{
	gl_Position = model * inPos;
}
