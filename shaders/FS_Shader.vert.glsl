#version 450 core

//Render Modes
#define RENDER_DEFAULT 0x00
#define RENDER_COLORFILL_ONLY 0x01

layout(location = 0) in vec4 inPos;
layout(location = 1) in vec3 inNor;
layout(location = 2) in vec2 inUVs;
layout(location = 3) in vec3 inTan;
layout(location = 4) in vec3 inBit;

out vec3 Normal;
out vec2 UV;
out vec3 worldPos;

out VS_OUT_LIGHTS
{
	mat3 TBN;
};

uniform mat4 projView;
uniform mat4 model;

uniform vec3 camPos;

uniform float far_plane;

uniform uint renderType = RENDER_DEFAULT;

struct LightProperties
{
	vec4 lightColor;
	vec4 lightPosition; //This is computed as a light direction if the lightType is set to Directional Light
	vec4 coneDirection;

	uint lightType;

	float spotCosCutoff;
	float spotExponent;

	float constant;
	float linear;
	float quadratic;
};

layout(std430, binding = 1) buffer Lights
{
	uint numLights;
	LightProperties properties[];
}lights;

mat3 createTBN()
{
	if(renderType == RENDER_COLORFILL_ONLY)
	{
		return mat3(1.0);
	}

	vec3 T = normalize(vec3(model * vec4(inTan, 0.0)));
	vec3 B = normalize(vec3(model * vec4(inBit, 0.0)));
	vec3 N = normalize(vec3(model * vec4(inNor, 0.0)));
	mat3 TBN = mat3(T, B, N);
	return TBN;
}

void main()
{
	vec4 wp = model * inPos;
	TBN = transpose(createTBN());
	UV = vec2(inUVs.s, 1.0 - inUVs.t); //This needs to be flipped in t coord for the .obj files
	worldPos = vec3(wp);

	if(renderType == RENDER_COLORFILL_ONLY)
	{
		Normal = normalize(inNor); //Is normalization really required?
	}

	gl_Position = projView * wp;
}
