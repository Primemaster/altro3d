#version 450 core
in vec3 UVdir;

uniform samplerCube environmentMap;

out vec4 FragColor;

void main()
{
	vec3 envColor = texture(environmentMap, UVdir).rgb;

    //envColor = vec3(1.0, 0.0, 0.0);

    float gamma = 1.0;
	envColor = pow(envColor, vec3(1.0/gamma));
  
    FragColor = vec4(envColor, 1.0);
}