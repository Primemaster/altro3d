#version 450 core

in vec2 UV;

out vec4 fragColour;
uniform sampler2D billboard;

void main()
{
	fragColour = texture(billboard, UV);
	//fragColour = vec4(1.0);
}
