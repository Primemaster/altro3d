#version 450 core

layout(location = 0) in vec2 inPos;

uniform vec2 center;
uniform vec2 size;

out vec2 UV;

float mapRangef(float s, float a, float b)
{
	return -1.0 + (s - a) * 2.0 / (b - a);
}
float mapRangef(float s, float a, float b, float c, float d)
{
	return c + (s - a) * (d - c) / (b - a);
}

//Fix the perma 16:9 aspect ratio...

void main()
{
	vec2 centerSS = vec2(mapRangef(center.x, 0, 1280), mapRangef(center.y, 0, 720)); //Mapping size to NDC [-1, 1]
	vec2 sizeSS = vec2(mapRangef(size.x, 0, 1280, 0, 1), mapRangef(size.y, 0, 720, 0 ,1)); //Mapping size to [0, 1]
	UV = 0.5 * inPos + 0.5;
	vec2 screenSpace = inPos * 0.25 + vec2(-0.5, -0.5); //sizeSS + centerSS;
	gl_Position = vec4(screenSpace, 0.0, 1.0);
}
