#version 450 core

//Light types
#define DLIGHT 0x01
#define PLIGHT 0x02
#define SLIGHT 0x03 //TODO: Create the shader spot light code

//Render Modes
#define RENDER_DEFAULT 0x00
#define RENDER_COLORFILL_ONLY 0x01

in vec3 worldPos;
in vec3 Normal;
in vec2 UV;

in VS_OUT_LIGHTS
{
	mat3 TBN;
};

out vec4 fragColour;

uniform vec3 camPos;

uniform sampler2D albedoMap;
uniform sampler2D normalMap;
uniform sampler2D roughnessMap;
uniform sampler2D metalicMap;
uniform sampler2D emissiveMap;

uniform samplerCube skybox;

uniform sampler2D shadowMap[8];
uniform mat4 lightSpaceMatrix[8];

uniform samplerCube shadowMapCube[8];


uniform float far_plane;

uniform vec4 colorFill = vec4(0.0);
uniform float metalUni = 0.0;
uniform float roughUni = 1.0; //Using this a specular instead -- This needs to be fixed somehow

uniform uint renderType = RENDER_DEFAULT;

//TODO: Take care of shadow

struct LightProperties
{
	vec4 lightColor;
	vec4 lightPosition; //This is computed as a light direction if the lightType is set to Directional Light
	vec4 coneDirection;

	uint lightType;

	float spotCosCutoff;
	float spotExponent;

	float constant;
	float linear;
	float quadratic;
};

layout(std430, binding = 1) buffer Lights
{
	uint numLights;
	LightProperties properties[];
}lights;

float calculateShadowOcclusion(vec4 fragPosLightSpace, uint id, float NdotL)
{
	vec3 projCoords = fragPosLightSpace.xyz / fragPosLightSpace.w;
	projCoords = projCoords * 0.5 + 0.5;
	float currentDepth = projCoords.z;
	float shadow = 0.0;

	if(currentDepth > 1.0)
	{
		shadow = 0.0;
	}
	else
	{
		float closestDepth = texture(shadowMap[id], projCoords.xy).r;
		float bias = max(0.05 * (1.0 - NdotL), 0.005);
		shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;
	}


	return shadow;
}

float calculateShadowOcclusion(vec3 fragPos, vec3 lightPos, uint id)
{
	vec3 fragToLight = fragPos - lightPos;
	float closestDepth = texture(shadowMapCube[id], fragToLight).r;
	closestDepth *= far_plane;

	float currentDepth = length(fragToLight);

	float bias = 0.005; //max(0.05 * (1.0 - NdotL), 0.005);
	float shadow = currentDepth - bias > closestDepth ? 1.0 : 0.0;

	if(currentDepth > 1.0)
		shadow = 0.0;

	return shadow;
}

void main()
{
	vec3 V0 = normalize(camPos - worldPos);
	vec3 N = vec3(0.0);
	vec3 R = vec3(0.0);
	vec3 reflection = vec3(0.0);
	float metalness = 0.0;
	vec3 albedo = vec3(0.0);
	float roughness = 0.0;

	if(renderType == RENDER_DEFAULT)
	{
		N = normalize(texture(normalMap, UV).rgb * 2.0 - 1.0);
		R = reflect(V0, N);
		reflection = texture(skybox, R).rgb;

		metalness = texture(metalicMap, UV).r;
		albedo = pow(mix(texture(albedoMap, UV).rgb, reflection, metalness/1.2), vec3(2.2));
		roughness = texture(roughnessMap, UV).r;
	}
	else if(renderType == RENDER_COLORFILL_ONLY)
	{
		N = Normal;
		R = reflect(V0, N);
		reflection = texture(skybox, R).rgb;

		metalness = metalUni;
		albedo = pow(mix(colorFill.rgb, reflection, metalness/1.2), vec3(2.2));
		roughness = roughUni;
	}
	else
	{
		//Create a default error texture and model for error handling
	}

	vec3 finalColor = vec3(0.0);

	uint id0 = 0;
	uint id1 = 0;

	for(int i = 0; i < lights.numLights; i++)
	{
		vec3 L = vec3(0.0);

		if(lights.properties[i].lightType == DLIGHT)
		{
			L = TBN * normalize(lights.properties[i].lightPosition.xyz);
		}
		else
		{
			L = TBN * normalize(lights.properties[i].lightPosition.xyz - worldPos);
		}

		vec3 V = TBN * V0;

		vec3 H = normalize(L + V);

		float NdotL = dot(N, L);
		float diff = max(NdotL, 0.0);
		vec3 diffuse = diff * lights.properties[i].lightColor.xyz;
		
		float spec = pow(max(dot(N, H), 0.0), 32.0); //TODO: Create some not default material properties
		vec3 specular = spec * lights.properties[i].lightColor.xyz;

	    const float ambientStrength = 0.1;
	    vec3 ambient = ambientStrength * lights.properties[i].lightColor.xyz;

	    float distance = 1.0;
	    float attenuation = 1.0;

	    if(lights.properties[i].lightType != DLIGHT) //Check if the light is a point light
	    {
	    	distance = length(lights.properties[i].lightPosition.xyz - worldPos);
	    	attenuation = 1.0 / (lights.properties[i].constant + 
	    								lights.properties[i].linear * distance + 
	    								lights.properties[i].quadratic * (distance * distance));

	    	if(lights.properties[i].lightType == SLIGHT)
	    	{
	    		float spotCos = dot(L, -lights.properties[i].coneDirection.xyz);
	    		if(spotCos < lights.properties[i].spotCosCutoff)
	    		{
	    			attenuation = 0.0;
	    		}
	    		else
	    		{
	    			attenuation *= pow(spotCos, lights.properties[i].spotExponent);
	    		}
	    	}
	    }

	    ambient  *= attenuation;
	    diffuse  *= attenuation;
	    specular *= attenuation;


	    //To Implement : Variance Shadow Mapping! VSM - http://developer.download.nvidia.com/SDK/10.5/direct3d/Source/VarianceShadowMapping/Doc/VarianceShadowMapping.pdf
	    float occlusion = 0.0;
	    if(lights.properties[i].lightType == DLIGHT)
	    {
	   		occlusion = calculateShadowOcclusion(lightSpaceMatrix[id0] * vec4(worldPos, 1.0), id0, NdotL);
	   		id0++;
	    }
	   	else if(lights.properties[i].lightType == PLIGHT)
	   	{
	   		occlusion = calculateShadowOcclusion(worldPos, lights.properties[id1].lightPosition.xyz, id1);
	   		id1++;
	   	}
	   	else
	   	{
	   		//TODO: SpotLight
	   	}

	    //Ambient light not affected -> this is not a real PBR
	    vec3 result = (ambient + diffuse * (1.0 - occlusion)) * albedo + specular * roughness * (1.0 - occlusion);
	    finalColor += result;
	}
	
	float gamma = 2.2;
	vec3 emissive = vec3(0.0);
	emissive = texture(emissiveMap, UV).rgb;

	fragColour = vec4(pow(finalColor + 0.8 * emissive, vec3(1.0/gamma)), 1.0);
}
