#include "PyAltro.h"

Altro3D::StateManager * StateManager_new(unsigned int width, unsigned int height, const char * name)
{
	return new Altro3D::StateManager(width, height, name);
}

void StateManager_cleanup(Altro3D::StateManager * obj)
{
	obj->cleanup();
}

void StateManager_pushState(Altro3D::StateManager * obj, Altro3D::Scene * state)
{
	obj->pushState(state);
}

void StateManager_changeState(Altro3D::StateManager * obj, Altro3D::Scene * state)
{
	obj->changeState(state);
}

void StateManager_popState(Altro3D::StateManager * obj)
{
	obj->popState();
}

void StateManager_handleEvents(Altro3D::StateManager * obj)
{
	obj->handleEvents();
}

void StateManager_update(Altro3D::StateManager * obj)
{
	obj->update();
}

void StateManager_draw(Altro3D::StateManager * obj)
{
	obj->draw();
}

bool StateManager_running(Altro3D::StateManager * obj)
{
	return obj->running();
}

void StateManager_quit(Altro3D::StateManager * obj)
{
	obj->quit();
}
