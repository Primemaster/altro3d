#pragma once
#include "../../src/Altro3D"
//Extensive list for Python support extensions

extern "C"
{
	AL3D_API Altro3D::StateManager* StateManager_new(unsigned int width, unsigned int height, const char* name);
	AL3D_API void StateManager_cleanup(Altro3D::StateManager* obj);
	AL3D_API void StateManager_pushState(Altro3D::StateManager* obj, Altro3D::Scene* state);
	AL3D_API void StateManager_changeState(Altro3D::StateManager* obj, Altro3D::Scene* state);
	AL3D_API void StateManager_popState(Altro3D::StateManager* obj);
	AL3D_API void StateManager_handleEvents(Altro3D::StateManager* obj);
	AL3D_API void StateManager_update(Altro3D::StateManager* obj);
	AL3D_API void StateManager_draw(Altro3D::StateManager* obj);
	AL3D_API bool StateManager_running(Altro3D::StateManager* obj);
	AL3D_API void StateManager_quit(Altro3D::StateManager* obj);
}