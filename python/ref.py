from ctypes import cdll

glfw   = cdll.LoadLibrary('../Release/glfw3.dll          ')
glew   = cdll.LoadLibrary('../Release/glew32.dll         ')
assimp = cdll.LoadLibrary('../Release/assimp-vc140-mt.dll')
zlib   = cdll.LoadLibrary('../Release/zlib.dll           ')
AL3D   = cdll.LoadLibrary('../Release/Altro3D.dll        ')

class State(object):
	def __init__(self):
		self.obj = AL3D.DefState_new()
	def initialize():
		pass
	def cleanup():
		pass
	def pause():
		pass
	def resume():
		pass
	def handleEvents():
		pass
	def update():
		pass
	def draw():
		pass

class StateManager(object):
	def __init__(self, width, height, name):
		self.obj = AL3D.StateManager_new(width, height, name)

	def cleanup(self):
		AL3D.StateManager_cleanup(self.obj)

	def pushState(self, state):
		AL3D.StateManager_pushState(self.obj, state)

	def changeState(self, state):
		AL3D.StateManager_changeState(self.obj, state)

	def popState(self):
		AL3D.StateManager_popState(self.obj)

	def handleEvents(self):
		AL3D.StateManager_handleEvents(self.obj)

	def update(self):
		AL3D.StateManager_update(self.obj)

	def draw(self):
		AL3D.StateManager_draw(self.obj)

	def running(self):
		AL3D.StateManager_running(self.obj)

	def quit(self):
		AL3D.StateManager_quit(self.obj)

#Strings need to be passed as bytes so C understands
s1 = State()
SManager = StateManager(800, 600, b"Hello world!")
SManager.pushState(s1)

#while True:
#	SManager.handleEvents()
#	SManager.update()
#	SManager.draw()
input('Press Enter to continue...')

SManager.cleanup()