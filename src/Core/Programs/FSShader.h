#pragma once
#include "Program.h"

class FSShader : public Program
{
public:
	FSShader();
	~FSShader() = default;

	void setProjViewMatrix(const glm::mat4& matrix);
	void setModelMatrix(const glm::mat4& matrix);
	void setCameraPosition(const glm::vec3& pos);
	void setLightSpaceMatrixArray(const glm::mat4* matrizes, const unsigned int count);
	void setTextureUnit(const char* uniform, GLuint value);
	void setTextureUnitArray(const char* uniform, unsigned int count, int * const values);
	void setFarPlane(const float value);
	void setRenderType(const unsigned int value);
	void setModelColorFill(const glm::vec4& vector);
	void setRoughnessSingle(const float value);
	void setMetalnessSingle(const float value);

protected:
	void getUniformLocations() override;

private:
	GLuint loc_projViewMatrix = 0;
	GLuint loc_modelMatrix = 0;
	GLuint loc_camPos = 0;
	GLuint loc_farPlane = 0;
	GLuint loc_rtype = 0;
	GLuint loc_colorFill = 0;
	GLuint loc_roughnessSingle = 0;
	GLuint loc_metalnessSingle = 0;
};

