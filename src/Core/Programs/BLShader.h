#pragma once
#include "Program.h"

class BLShader : public Program
{
public:
	BLShader();
	~BLShader() = default;

	void setTextureUnit(const char* uniform, GLuint value);
	void setCenter(glm::vec2 center);
	void setSize(glm::vec2 size);

protected:
	void getUniformLocations() override;

private:
	GLuint loc_center = 0;
	GLuint loc_size = 0;

};

