#pragma once
#include "Program.h"

class SkyShader : public Program
{
public:
	SkyShader();
	~SkyShader() = default;

	void setProjMatrix(const glm::mat4& matrix);
	void setViewMatrix(const glm::mat4& matrix);
	void setTextureUnit(const char* uniform, GLuint value);

protected:
	void getUniformLocations() override;

private:
	GLuint loc_projMatrix = 0;
	GLuint loc_viewMatrix = 0;
};

