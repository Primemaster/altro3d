#pragma once
#include "Program.h"

class SMShader : public Program
{
public:
	SMShader();
	~SMShader() = default;

	void setLightSpaceMatrix(const glm::mat4& matrix);
	void setModelMatrix(const glm::mat4& matrix);

protected:
	void getUniformLocations() override;

private:
	GLuint loc_lightSpaceMatrix = 0;
	GLuint loc_modelMatrix = 0;
	GLuint loc_camPos = 0;
};

