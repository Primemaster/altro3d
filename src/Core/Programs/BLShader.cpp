#include "BLShader.h"

BLShader::BLShader() : Program("BL_Shader", "BL_Shader")
{
	getUniformLocations();
}

void BLShader::getUniformLocations()
{
	loc_center = glGetUniformLocation(getID(), "center");
	loc_size = glGetUniformLocation(getID(), "size");
}

void BLShader::setTextureUnit(const char * uniform, GLuint value)
{
	loadInt(glGetUniformLocation(getID(), uniform), value);
}

void BLShader::setCenter(glm::vec2 center)
{
	loadVector2f(loc_center, center);
}

void BLShader::setSize(glm::vec2 size)
{
	loadVector2f(loc_size, size);
}
