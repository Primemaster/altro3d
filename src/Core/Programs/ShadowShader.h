#pragma once
#include "Program.h"

class ShadowShader : public Program
{
public:
	ShadowShader();
	~ShadowShader() = default;

	void setProjViewMatrix(const glm::mat4& matrix);
	void setModelMatrix(const glm::mat4& matrix);
	void setCameraPosition(const glm::vec3& pos);
	void setTextureUnit(const char* uniform, GLuint value);

protected:
	void getUniformLocations() override;

private:
	GLuint loc_projViewMatrix = 0;
	GLuint loc_modelMatrix = 0;
	GLuint loc_camPos = 0;
};

