#include "SMShader.h"



SMShader::SMShader() : Program("SM_Shader", "SM_Shader")
{
	getUniformLocations();
}

void SMShader::setLightSpaceMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_lightSpaceMatrix, matrix);
}

void SMShader::setModelMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_modelMatrix, matrix);
}

void SMShader::getUniformLocations()
{
	loc_lightSpaceMatrix = glGetUniformLocation(getID(), "lightSpace");
	loc_modelMatrix = glGetUniformLocation(getID(), "model");
}