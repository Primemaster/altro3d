#pragma once
#include "../../AL3D_EXP.h"
#include "../Log.h"

#include <iostream>
#include <GL/glew.h>
#include <glm/glm.hpp>

class Program
{
public:
	Program(const std::string& vertexShaderFile, const std::string& fragmentShaderFile);
	Program(const std::string& vertexShaderFile, const std::string& fragmentShaderFile, const std::string& geometryShaderFile);
	virtual ~Program();

	void bind();
	void unbind();
	GLuint getID() const;

protected:
	virtual void getUniformLocations() = 0;


	void loadInt(GLuint location, int value);
	void loadUInt(GLuint location, unsigned int value);
	void loadFloat(GLuint location, float value);
	void loadVector2f(GLuint location, const glm::vec2 vector);
	void loadVector3f(GLuint location, const glm::vec3 vector);
	void loadVector4f(GLuint location, const glm::vec4 vector);
	void loadMatrix4f(GLuint location, const glm::mat4 matrix);

	void loadMatrix4fArray(GLuint location, unsigned int count, const glm::mat4* matrizes);
	void loadIntArray(GLuint location, unsigned int count, int* const values);

private:
	GLuint m_programID;
};

