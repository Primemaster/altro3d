#pragma once
#include "Program.h"

class SM_CBShader : public Program
{
public:
	SM_CBShader();
	~SM_CBShader() = default;

	void setLightSpaceMatrixArray(const glm::mat4* matrizes);
	void setModelMatrix(const glm::mat4& matrix);
	void setLightPos(const glm::vec3& vector);
	void setFarPlane(const float value);

protected:
	void getUniformLocations() override;

private:
	GLuint loc_lightSpaceMatrix = 0;
	GLuint loc_modelMatrix = 0;
	GLuint loc_camPos = 0;
	GLuint loc_lightPos = 0;
	GLuint loc_farPlane = 0;
};

