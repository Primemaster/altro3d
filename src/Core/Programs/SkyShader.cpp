#include "SkyShader.h"
#include "../Log.h"


SkyShader::SkyShader() : Program("SkyBox", "SkyBox")
{
	getUniformLocations();
}

void SkyShader::getUniformLocations()
{
	loc_projMatrix = glGetUniformLocation(getID(), "projection");
	loc_viewMatrix = glGetUniformLocation(getID(), "view");
	FILE_LOG(logDEBUG) << "P:" << loc_projMatrix << " V:" << loc_viewMatrix;
}

void SkyShader::setProjMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_projMatrix, matrix);
}

void SkyShader::setViewMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_viewMatrix, matrix);
}

void SkyShader::setTextureUnit(const char * uniform, GLuint value)
{
	loadInt(glGetUniformLocation(getID(), uniform), value);
}
