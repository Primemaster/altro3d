#include "Program.h"
#include "../loader.h"

Program::Program(const std::string & vertexShaderFile, const std::string & fragmentShaderFile)
	: m_programID(loader::loadShader(vertexShaderFile, fragmentShaderFile))
{
	bind();
	FILE_LOG(logDEBUG) << "Program compiled and linked successfully. ID [" << m_programID << "]";
}

Program::Program(const std::string & vertexShaderFile, const std::string & fragmentShaderFile, const std::string & geometryShaderFile)
	: m_programID(loader::loadShader(vertexShaderFile, fragmentShaderFile, geometryShaderFile))
{
	bind();
	FILE_LOG(logDEBUG) << "Program compiled and linked successfully. ID [" << m_programID << "]";
}

Program::~Program()
{
	glDeleteProgram(m_programID);
}

void Program::bind()
{
	glUseProgram(m_programID);
}

void Program::unbind()
{
	glUseProgram(0);
}

GLuint Program::getID() const
{
	return m_programID;
}

void Program::loadInt(GLuint location, int value)
{
	glUniform1i(location, value);
}

void Program::loadUInt(GLuint location, unsigned int value)
{
	glUniform1ui(location, value);
}

void Program::loadFloat(GLuint location, float value)
{
	glUniform1f(location, value);
}

void Program::loadVector2f(GLuint location, const glm::vec2 vector)
{
	glUniform2f(location, vector.x, vector.y);
}

void Program::loadVector3f(GLuint location, const glm::vec3 vector)
{
	glUniform3f(location, vector.x, vector.y, vector.z);
}

void Program::loadVector4f(GLuint location, const glm::vec4 vector)
{
	glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
}

void Program::loadMatrix4f(GLuint location, const glm::mat4 matrix)
{
	glUniformMatrix4fv(location, 1, GL_FALSE, &matrix[0][0]);
}

void Program::loadMatrix4fArray(GLuint location, unsigned int count, const glm::mat4 * matrizes)
{
	glUniformMatrix4fv(location, count, GL_FALSE, &matrizes[0][0][0]);
}

void Program::loadIntArray(GLuint location, unsigned int count, int * const values)
{
	glUniform1iv(location, count, values);
}
