#include "ShadowShader.h"



ShadowShader::ShadowShader() : Program("ShadowShader", "ShadowShader")
{
	getUniformLocations();
}

void ShadowShader::setProjViewMatrix(const glm::mat4 & matrix)
{
}

void ShadowShader::setModelMatrix(const glm::mat4 & matrix)
{
}

void ShadowShader::setCameraPosition(const glm::vec3 & pos)
{
}

void ShadowShader::setTextureUnit(const char * uniform, GLuint value)
{
}

void ShadowShader::getUniformLocations()
{
}
