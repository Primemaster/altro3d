#include "FSShader.h"
#include "../Log.h"


FSShader::FSShader() : Program("FS_Shader", "FS_Shader")
{
	getUniformLocations();
}

void FSShader::getUniformLocations()
{
	loc_projViewMatrix = glGetUniformLocation(getID(), "projView");
	loc_modelMatrix = glGetUniformLocation(getID(), "model");
	loc_camPos = glGetUniformLocation(getID(), "camPos");
	loc_farPlane = glGetUniformLocation(getID(), "far_plane");
	loc_rtype = glGetUniformLocation(getID(), "renderType");
	loc_colorFill = glGetUniformLocation(getID(), "colorFill");
	loc_roughnessSingle = glGetUniformLocation(getID(), "roughUni");
	loc_metalnessSingle = glGetUniformLocation(getID(), "metalUni");
	FILE_LOG(logDEBUG) << "PV:" << loc_projViewMatrix << " M:" << loc_modelMatrix << " CP:" << loc_camPos << " FP:" << loc_farPlane;
}

void FSShader::setProjViewMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_projViewMatrix, matrix);
}

void FSShader::setModelMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_modelMatrix, matrix);
}

void FSShader::setCameraPosition(const glm::vec3 & pos)
{
	loadVector3f(loc_camPos, pos);
}

void FSShader::setLightSpaceMatrixArray(const glm::mat4 * matrizes, const unsigned int count)
{
	loadMatrix4fArray(glGetUniformLocation(getID(), "lightSpaceMatrix"), count, matrizes);
}

void FSShader::setTextureUnit(const char * uniform, GLuint value)
{
	loadInt(glGetUniformLocation(getID(), uniform), value);
}

void FSShader::setTextureUnitArray(const char * uniform, unsigned int count, int * const values)
{
	loadIntArray(glGetUniformLocation(getID(), uniform), count, values);
}

void FSShader::setFarPlane(const float value)
{
	loadFloat(loc_farPlane, value);
}

void FSShader::setRenderType(const unsigned int value)
{
	loadUInt(loc_rtype, value);
}

void FSShader::setModelColorFill(const glm::vec4 & vector)
{
	loadVector4f(loc_colorFill, vector);
}

void FSShader::setRoughnessSingle(const float value)
{
	loadFloat(loc_roughnessSingle, value);
}

void FSShader::setMetalnessSingle(const float value)
{
	loadFloat(loc_metalnessSingle, value);
}
