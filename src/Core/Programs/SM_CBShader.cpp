#include "SM_CBShader.h"



SM_CBShader::SM_CBShader() : Program("SM_CB_Shader", "SM_CB_Shader", "SM_CB_Shader")
{
	getUniformLocations();
}

void SM_CBShader::setLightSpaceMatrixArray(const glm::mat4 * matrizes)
{
	loadMatrix4fArray(loc_lightSpaceMatrix, 6, matrizes);
}

void SM_CBShader::setModelMatrix(const glm::mat4 & matrix)
{
	loadMatrix4f(loc_modelMatrix, matrix);
}

void SM_CBShader::setLightPos(const glm::vec3 & vector)
{
	loadVector3f(loc_lightPos, vector);
}

void SM_CBShader::setFarPlane(const float value)
{
	loadFloat(loc_farPlane, value);
}

void SM_CBShader::getUniformLocations()
{
	loc_lightSpaceMatrix = glGetUniformLocation(getID(), "lightSpace");
	loc_modelMatrix = glGetUniformLocation(getID(), "model");
	loc_lightPos = glGetUniformLocation(getID(), "lightPos");
	loc_farPlane = glGetUniformLocation(getID(), "far_plane");
}