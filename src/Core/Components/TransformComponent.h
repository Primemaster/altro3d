#pragma once
#include "../../AL3D_EXP.h"

#include "../Component.h"
#include <glm\glm.hpp>
#include <glm\gtc\quaternion.hpp>

namespace Altro3D
{
	class AL3D_API TransformComponent : public Component
	{
	public:
		TransformComponent() = default;
		~TransformComponent() override = default;

		void setTranslation(const glm::vec3& translation);
		void setScale(const float scale);

		//glm::quat& getRotationQuat();
		glm::vec3& getRotation();
		glm::vec3& getTranslation();
		glm::vec3& getScale();

		glm::mat4 getModelMatrix();

	private:
		glm::vec3 position = glm::vec3(0.0f);
		glm::vec3 scale = glm::vec3(1.0f);
		glm::vec3 rotation = glm::vec3(0.0f);
	};
}

