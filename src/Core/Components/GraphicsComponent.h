#pragma once
#include "../Component.h"
#include "../../App/ResourceManager.h"

#include <iostream>
#include <vector>
#include <map>

#include <GL\glew.h>

namespace Altro3D
{
	class GraphicsComponent : public Component
	{
	public:
		enum tSlot : int;
		enum rType : int;

		typedef rType RenderMode;

		//TODO: Append model / Append Texture
		AL3D_API GraphicsComponent(Altro3D::ResourceManager * rm, const char* modelName);
		AL3D_API GraphicsComponent(Altro3D::ResourceManager * rm, const char* modelName, glm::vec4 fillColor);
		AL3D_API GraphicsComponent(RenderMode renderMode, Altro3D::ResourceManager * rm = nullptr, const char* modelName = nullptr, glm::vec4 fillColor = glm::vec4(1.0f));
		AL3D_API ~GraphicsComponent() override;

		unsigned int getDrawSize() const;
		AL3D_API glm::vec4 getModelFillColor() const;
		void bindVertexArrayObject(unsigned int currentMesh);
		void bindTextureObject(tSlot texture, GLuint type);
		unsigned int getIndexSize(unsigned int currentMesh);

		AL3D_API void addTexture2D(Altro3D::ResourceManager* rm, const char* name, tSlot slot);
		AL3D_API void addTextureCube(Altro3D::ResourceManager* rm, const char * names[], tSlot slot);

		void addTexture2D_GLInternal(GLuint texture, tSlot slot);

		AL3D_API enum tSlot
		{
			TEX0, TEX1, TEX2, TEX3, TEX4, TEX5, TEX6,
			TEX7, TEX8, TEX9, TEX10, TEX11, TEX12, TEX13,
			TEX14, TEX15, TEX16, TEX17, TEX18, TEX19, TEX20,
			TEX21, TEX22, TEX23, TEX24, TEX25, TEX26, TEX27, T_SIZE
		};

		AL3D_API enum rType
		{
			RENDER_DEFAULT, RENDER_COLORFILL_ONLY, RENDER_BILLBOARD
		};

		rType rendertype = RENDER_DEFAULT;

	private:
		void addModel(Altro3D::ResourceManager* rm, const char* name);
		void addBLModel();

		enum bType
		{
			VAO, VBO, EBO, NRM, UVS, TAN, BTN, CNT, IDC, B_SIZE
		};


		struct OGLBuffer
		{
			GLuint buffers[bType::B_SIZE];
		};

		//Once again this is only designed for a single batch of distinct texture types for a model
		struct OGLTexture
		{
			GLuint textures[tSlot::T_SIZE] = { 0 };
		}textures;

		glm::vec4 modelColor = glm::vec4(0.0);

		std::map<unsigned int, OGLBuffer> buffers;
	};
}
