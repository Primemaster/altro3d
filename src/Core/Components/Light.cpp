﻿#include "Light.h"
#include <iostream>

namespace Altro3D
{
	Light::Light(unsigned int lightType, float maxDistance)
	{
		properties.lightType = lightType;
		properties.constant = 1.0f;
		properties.linear = 4.6905f * powf(maxDistance, -1.01f);
		properties.quadratic = 82.445f * powf(maxDistance, -2.019f);
	}
	void Light::setColor(glm::vec3 lightcolor)
	{
		properties.lightColor = glm::vec4(lightcolor, 1.0f);
		this->static_ = true;
	}
	void Light::setPosition(glm::vec3 lightposition)
	{
		properties.lightPosition = glm::vec4(lightposition, 1.0f);
		this->static_ = true;
	}
	void Light::setCone(glm::vec3 coneDirection)
	{
		properties.coneDirection = glm::vec4(coneDirection, 1.0f);
		this->static_ = true;
	}
	void Light::setSpotCosCutoff(float sccutoff)
	{
		properties.spotCosCutoff = sccutoff;
		this->static_ = true;
	}
	void Light::setSpotExponent(float sexponent)
	{
		properties.spotExponent = sexponent;
		this->static_ = true;
	}
	void Light::setMaxDistance(float maxDistance)
	{
		properties.linear = 4.6905f * powf(maxDistance, -1.01f);
		properties.quadratic = 82.445f * powf(maxDistance, -2.019f);
		this->static_ = true;
	}
	bool Light::changed()
	{
		return static_;
	}

	bool Light::isDirectional()
	{
		return (properties.lightType == 1);
	}

	//UNDONE: Delete this... [Deprecated]
	void Light::bindBufferObject()
	{
		//glBindBuffer(GL_SHADER_STORAGE_BUFFER, shaderstorage.ssbo);
	}

	void Light::loadInShader(std::vector<Altro3D::Light*> * lights)
	{
		if (lights == nullptr)
		{
			//TODO: Generate light nullptr error
			return;
		}

		static GLightProperties* glp = nullptr;

		bool diffSize = false;
		static unsigned int lastSize = 0;
		if (lights->size() != lastSize)
		{
			lastSize = lights->size();
			if (glp != nullptr)
			{
				free(glp);
			}

			glp = (GLightProperties*)malloc(sizeof(GLightProperties) + sizeof(LightProperties) * lastSize);

			if (glp == nullptr)
			{
				//TODO: Generate nullptr exception for GLP inside loop
				return;
			}

			glp->numLights = lastSize;
			diffSize = true;
		}

		if (glp == nullptr)
		{
			//TODO: Generate nullptr exception for GLP outside loop
			return;
		}

		bool oneChanged = false;

		unsigned int i = 0;
		for (auto l : *lights)
		{
			if (l->changed() || diffSize)
			{
				glp->properties[i] = l->properties;
				l->static_ = false;
				oneChanged = true;
			}
			else
			{
				continue;
			}
			i++;
		}

		static GLuint ssbo = 0;

		if (ssbo == 0)
		{
			glGenBuffers(1, &ssbo);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
			glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(GLightProperties) + sizeof(LightProperties) * lastSize, glp, GL_DYNAMIC_COPY);
			glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, ssbo);
			glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
		}
		else
		{
			if (diffSize || oneChanged)
			{
				glBindBuffer(GL_SHADER_STORAGE_BUFFER, ssbo);
				glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(GLightProperties) + sizeof(LightProperties) * lastSize, glp, GL_DYNAMIC_COPY);
				glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
			}
		}
	}
}
