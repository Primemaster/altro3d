#include "TransformComponent.h"
#include <glm\gtc\matrix_transform.hpp>

namespace Altro3D
{
	void TransformComponent::setTranslation(const glm::vec3 & translation)
	{
		position = translation;
	}

	void TransformComponent::setScale(const float scale)
	{
		this->scale = glm::vec3(scale);
	}

	glm::vec3 & TransformComponent::getRotation()
	{
		return rotation;
	}

	glm::vec3 & TransformComponent::getTranslation()
	{
		return position;
	}

	glm::vec3 & TransformComponent::getScale()
	{
		return scale;
	}
	glm::mat4 TransformComponent::getModelMatrix()
	{
		glm::mat4 local_rotate = glm::rotate(glm::mat4(1.0f), glm::radians(rotation.x), glm::vec3(1.0f, 0.0f, 0.0f)); //X-Axis Rotation
		local_rotate = glm::rotate(local_rotate, glm::radians(rotation.y), glm::vec3(0.0f, 1.0f, 0.0f)); //Y-Axis Rotation
		local_rotate = glm::rotate(local_rotate, glm::radians(rotation.z), glm::vec3(0.0f, 0.0f, 1.0f)); //Z-Axis Rotation

		glm::mat4 local_translate = glm::translate(glm::mat4(1.0f), position);

		glm::mat4 local_scale = glm::scale(glm::mat4(1.0f), scale);

		return local_translate * local_rotate * local_scale;
	}
}