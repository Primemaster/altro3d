#pragma once
#include "..\Component.h"
#include <glm\glm.hpp>
#include <vector>

#include <GL\glew.h>

constexpr auto DLIGHT = 0x01;
constexpr auto PLIGHT = 0x02;
constexpr auto SLIGHT = 0x03;

constexpr auto MAX_LIGHTS = 128;

namespace Altro3D
{
	class AL3D_API Light : public Component
	{
	public:
		friend class ShadowRenderer;
		Light(unsigned int lightType, float maxDistance);
		~Light() = default;

		void setColor(glm::vec3 lightcolor);
		void setPosition(glm::vec3 lightposition);
		void setCone(glm::vec3 coneDirection);
		void setSpotCosCutoff(float sccutoff);
		void setSpotExponent(float sexponent);
		void setMaxDistance(float maxDistance);

		bool changed();

		bool isDirectional();

		static void bindBufferObject();

		static void loadInShader(std::vector<Altro3D::Light*>* lights);

	private:
		struct alignas(16) LightProperties
		{
			glm::vec4 lightColor = glm::vec4(1.0);
			glm::vec4 lightPosition = glm::vec4(1.0); //This is computed as a light direction if the lightType is set to Directional Light
			glm::vec4 coneDirection = glm::vec4(0.0);

			unsigned int lightType = 1;

			float spotCosCutoff = 0.1f;
			float spotExponent = 0.5f;

			float constant = 1.0f; //This defaults to arround 100 meters
			float linear = 0.045f;
			float quadratic = 0.0075f;
		}properties;

		bool isDirect = false;

#pragma warning( push )
#pragma warning(disable : 4200)  
		struct GLightProperties
		{
			unsigned int numLights;
			LightProperties properties[];
		};
#pragma warning( pop )
		bool static_ = true;
	};
}


