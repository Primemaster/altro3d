#include "GraphicsComponent.h"
#include <stb_image.h>

namespace Altro3D
{
	GraphicsComponent::GraphicsComponent(Altro3D::ResourceManager * rm, const char* modelName)
	{
		addModel(rm, modelName);
		rendertype = RENDER_DEFAULT;
	}

	GraphicsComponent::GraphicsComponent(Altro3D::ResourceManager * rm, const char * modelName, glm::vec4 fillColor)
	{
		addModel(rm, modelName);
		modelColor = fillColor;
		rendertype = RENDER_COLORFILL_ONLY;
	}

	GraphicsComponent::GraphicsComponent(RenderMode renderMode, Altro3D::ResourceManager * rm, const char * modelName, glm::vec4 fillColor)
	{
		switch (renderMode)
		{
		case RENDER_DEFAULT:
			addModel(rm, modelName);
			rendertype = RENDER_DEFAULT;
			break;
		case RENDER_COLORFILL_ONLY:
			addModel(rm, modelName);
			modelColor = fillColor;
			rendertype = RENDER_COLORFILL_ONLY;
			break;
		case RENDER_BILLBOARD:
			addBLModel();
			rendertype = RENDER_BILLBOARD;
			break;
		default:
			FILE_LOG(logERROR) << "No such RenderMode - 0x" << std::hex << static_cast<unsigned int>(renderMode);
			break;
		}
	}

	GraphicsComponent::~GraphicsComponent()
	{
		for (auto a : buffers) //TODO: Delete the textures too
		{
			glDeleteVertexArrays(1, &a.second.buffers[bType::VAO]);

			glDeleteBuffers(1, &a.second.buffers[bType::VBO]);
			glDeleteBuffers(1, &a.second.buffers[bType::EBO]);
			glDeleteBuffers(1, &a.second.buffers[bType::NRM]);
			glDeleteBuffers(1, &a.second.buffers[bType::UVS]);
			glDeleteBuffers(1, &a.second.buffers[bType::TAN]);
			glDeleteBuffers(1, &a.second.buffers[bType::BTN]);
		}
		buffers.clear();
	}

	unsigned int GraphicsComponent::getDrawSize() const
	{
		return buffers.size();
	}

	glm::vec4 GraphicsComponent::getModelFillColor() const
	{
		return modelColor;
	}

	void GraphicsComponent::bindVertexArrayObject(unsigned int currentMesh)
	{
		glBindVertexArray(buffers[currentMesh].buffers[VAO]);
	}

	void GraphicsComponent::bindTextureObject(tSlot texture, GLuint type)
	{
		glBindTexture(type, textures.textures[texture]);
	}

	unsigned int GraphicsComponent::getIndexSize(unsigned int currentMesh)
	{
		return buffers[currentMesh].buffers[IDC];
	}

	void GraphicsComponent::addTexture2D(Altro3D::ResourceManager * rm, const char * name, tSlot slot)
	{
		//Out of the loop once again because of the direct texture <-> model correspondence
		loader::ImageData* image = rm->images[name];

		auto lambda = [&](GLint format) -> void
		{
			//TODO: Togglable Alpha channel
			switch (image->tag)
			{
			case loader::ImageData::TAG::FLOAT:
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, image->width, image->height, 0, GL_RGB, GL_FLOAT, image->dtype.dataf);
				//stbi_image_free(image->dtype.dataf); //TODO: The resource manager should be the one to handle the image destruction
				break;
			case loader::ImageData::TAG::BYTE:
				glTexImage2D(GL_TEXTURE_2D, 0, format, image->width, image->height, 0, format, GL_UNSIGNED_BYTE, image->dtype.datab);
				//stbi_image_free(image->dtype.datab); //TODO: The resource manager should be the one to handle the image destruction
				break;
			default:
				break;
			}
		};

		GLuint tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_2D, tex);

		switch (image->nrComponents) //Should use floating point format for the extra textures?
		{
		case 1:
			lambda(GL_RED);
			break;
		case 2:
			lambda(GL_RG);
			break;
		case 3:
			lambda(GL_RGB);
			break;
		case 4:
			lambda(GL_RGBA);
			break;
		default:
			FILE_LOG(logERROR) << "Image [UID: " << TO_UINTPTR_T(image) << "] internal format not recognized. Possible formats are: GL_RED | GL_RG | GL_RGB | GL_RGBA";
			break;
		}

		//Assigning the basic parameter for now independent of type
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		//Duplicate textures should not be a problem since they are handled in the ResourceManager class
		//TODO: Handle texture existance for the shader
		textures.textures[slot] = tex;

		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GraphicsComponent::addTextureCube(Altro3D::ResourceManager * rm, const char * names[], tSlot slot)
	{
		//Out of the loop once again because of the direct texture <-> model correspondence
		loader::ImageData* images[6];
		images[0] = rm->images[names[0]]; //Right
		images[1] = rm->images[names[1]]; //Left
		images[2] = rm->images[names[2]]; //Top
		images[3] = rm->images[names[3]]; //Bottom
		images[4] = rm->images[names[4]]; //Back
		images[5] = rm->images[names[5]]; //Front

		auto lambda = [&](GLint format, unsigned int i) -> void
		{
			//TODO: Togglable Alpha channel
			switch (images[i]->tag)
			{
			case loader::ImageData::TAG::FLOAT:
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, images[i]->width, images[i]->height, 0, GL_RGB, GL_FLOAT, images[i]->dtype.dataf);
				//stbi_image_free(images[i]->dtype.dataf); //TODO: The resource manager should be the one to handle the image destruction
				break;
			case loader::ImageData::TAG::BYTE:
				glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, images[i]->width, images[i]->height, 0, format, GL_UNSIGNED_BYTE, images[i]->dtype.datab);
				//stbi_image_free(images[i]->dtype.datab); //TODO: The resource manager should be the one to handle the image destruction
				break;
			default:
				break;
			}
		};

		GLuint tex;
		glGenTextures(1, &tex);
		glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

		for (unsigned int i = 0; i < 6; i++)
		{
			switch (images[i]->nrComponents) //Should use floating point format for the extra textures?
			{
			case 1:
				lambda(GL_RED, i);
				break;
			case 2:
				lambda(GL_RG, i);
				break;
			case 3:
				lambda(GL_RGB, i);
				break;
			case 4:
				lambda(GL_RGBA, i);
				break;
			default:
				FILE_LOG(logERROR) << "Image [UID: " << TO_UINTPTR_T(images[i]) << "] internal format not recognized. Possible formats are: GL_RED | GL_RG | GL_RGB | GL_RGBA";
				break;
			}
		}

		//Assigning the basic parameter for now independent of type
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

		//Duplicate textures should not be a problem since they are handled in the ResourceManager class
		//TODO: Handle texture existance for the shader
		textures.textures[slot] = tex;

		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	}

	void GraphicsComponent::addTexture2D_GLInternal(GLuint texture, tSlot slot)
	{
		textures.textures[slot] = texture;
	}

	void GraphicsComponent::addModel(Altro3D::ResourceManager * rm, const char* name)
	{
		std::vector<Mesh*> model = rm->models[name];
		unsigned int modelMeshCount = model.size();

		if (modelMeshCount != 0)
		{
			for (unsigned int i = 0; i < modelMeshCount; i++)
			{
				Mesh* ms = model[i];
				if (ms == nullptr)
				{
					//TODO: Null mesh exception
				}
				GLuint vao;
				GLuint vbo, ebo, nrm, uvs, tan, btn;
				glGenVertexArrays(1, &vao);
				glGenBuffers(1, &vbo);
				glGenBuffers(1, &ebo);
				glGenBuffers(1, &nrm);
				glGenBuffers(1, &uvs);
				glGenBuffers(1, &tan);
				glGenBuffers(1, &btn);

				glBindVertexArray(vao); //PV Atributes next

				glBindBuffer(GL_ARRAY_BUFFER, vbo);
				glBufferData(GL_ARRAY_BUFFER, ms->vertices.size() * sizeof(glm::vec3), &ms->vertices.front(), GL_STATIC_DRAW);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
				glEnableVertexAttribArray(0);

				if (!ms->normals.empty())
				{
					glBindBuffer(GL_ARRAY_BUFFER, nrm);
					glBufferData(GL_ARRAY_BUFFER, ms->normals.size() * sizeof(glm::vec3), &ms->normals.front(), GL_STATIC_DRAW);
					glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
					glEnableVertexAttribArray(1);
				}
				if (!ms->indices.empty())
				{
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, ms->indices.size() * sizeof(unsigned int), &ms->indices.front(), GL_STATIC_DRAW);
				}
				else
				{
					//TODO: Tag this model to render default mode
				}
				if (!ms->uvs.empty())
				{
					glBindBuffer(GL_ARRAY_BUFFER, uvs);
					glBufferData(GL_ARRAY_BUFFER, ms->uvs.size() * sizeof(glm::vec2), &ms->uvs.front(), GL_STATIC_DRAW);
					glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
					glEnableVertexAttribArray(2);
				}
				if (!ms->tangents.empty())
				{
					glBindBuffer(GL_ARRAY_BUFFER, tan);
					glBufferData(GL_ARRAY_BUFFER, ms->tangents.size() * sizeof(glm::vec3), &ms->tangents.front(), GL_STATIC_DRAW);
					glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
					glEnableVertexAttribArray(3);

					glBindBuffer(GL_ARRAY_BUFFER, btn);
					glBufferData(GL_ARRAY_BUFFER, ms->bitangents.size() * sizeof(glm::vec3), &ms->bitangents.front(), GL_STATIC_DRAW);
					glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
					glEnableVertexAttribArray(4);
				}

				glBindVertexArray(0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
				glBindBuffer(GL_ARRAY_BUFFER, 0);

				OGLBuffer buffer;
				buffer.buffers[bType::VAO] = vao;
				buffer.buffers[bType::VBO] = vbo;
				buffer.buffers[bType::EBO] = ebo;
				buffer.buffers[bType::NRM] = nrm;
				buffer.buffers[bType::UVS] = uvs;
				buffer.buffers[bType::TAN] = tan;
				buffer.buffers[bType::BTN] = btn;

				buffer.buffers[bType::CNT] = ms->vertices.size();
				buffer.buffers[bType::IDC] = ms->indices.size();

				buffers.emplace(i, buffer);
			}
		}
	}

	void GraphicsComponent::addBLModel()
	{
		const std::vector<glm::vec2> vertices = 
		{
			glm::vec2(1.0f, -1.0f),
			glm::vec2(1.0f, 1.0f),
			glm::vec2(-1.0f, 1.0f),
			glm::vec2(-1.0f, -1.0f)
		};
		const std::vector<unsigned int> indices = 
		{
			0, 1, 2, 0, 2, 3
		};

		GLuint vao;
		GLuint vbo, ebo;
		glGenVertexArrays(1, &vao);
		glGenBuffers(1, &vbo);
		glGenBuffers(1, &ebo);

		glBindVertexArray(vao); //PV Atributes next

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(glm::vec2), &vertices.front(), GL_STATIC_DRAW);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
		glEnableVertexAttribArray(0);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), &indices.front(), GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		OGLBuffer buffer;
		buffer.buffers[bType::VAO] = vao;
		buffer.buffers[bType::VBO] = vbo;
		buffer.buffers[bType::EBO] = ebo;

		buffer.buffers[bType::CNT] = vertices.size();
		buffer.buffers[bType::IDC] = indices.size();

		buffers.emplace(0, buffer);
	}
}