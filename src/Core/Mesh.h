#pragma once

#include <GL\glew.h>
#include <glm\glm.hpp>
#include <iostream>
#include <vector>

namespace Altro3D
{
	class ResourceManager;
	class GraphicsComponent;
}

class Mesh
{
public:
	friend class Altro3D::ResourceManager;
	friend class Altro3D::GraphicsComponent;

	Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, 
		std::vector<glm::vec3> normals, std::vector<glm::vec3> tangents = std::vector<glm::vec3>(),
		std::vector<glm::vec3> bitangents = std::vector<glm::vec3>(), std::vector<glm::vec2> uvs = std::vector<glm::vec2>());

	Mesh(std::vector<glm::vec3> vertices);

	~Mesh() = default;

private:
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;
	std::vector<glm::vec2> uvs;
	std::vector<unsigned int> indices;
};

