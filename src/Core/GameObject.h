#pragma once
#include "../AL3D_EXP.h"

#include <iostream>
#include <map>
#include <algorithm>
#include <cctype>
#include <cstdint>
#include <typeindex>

class Component;


namespace Altro3D
{
	class GraphicsComponent;
	class TransformComponent;
	class Light;

	class AL3D_API GameObject
	{
	public:
		GameObject();
		~GameObject();

		void addComponent(GraphicsComponent* component);
		void addComponent(TransformComponent* component);
		void addComponent(Light* component);

		void deleteComponent(GraphicsComponent* component);
		void deleteComponent(TransformComponent* component);
		void deleteComponent(Light* component);

		template<typename C>
		C* getComponent()
		{
			auto it = components.find(std::type_index(typeid(C)));
			if (it != components.end())
			{
				return dynamic_cast<C*>(it->second);
			}
			return nullptr;
		}

	private:
		std::map<std::type_index, Component*> components;
		const std::uintptr_t uid = reinterpret_cast<std::uintptr_t>(this);
	};
}

