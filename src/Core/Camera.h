#pragma once
#include "../AL3D_EXP.h"

#include <iostream>
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#define PI 3.1415926535897932
#undef near
#undef far

typedef unsigned int uint;

namespace Altro3D
{
	class AL3D_API Camera
	{
	public:
		Camera();
		~Camera() = default;

		void configure();
		void calculateHeadingRH(float pitchChange, float yawChange);
		void setEye(glm::vec3 position);

		inline const glm::vec3 getUpwards() const
		{
			return UP;
		}

		inline const glm::vec3 getEye() const
		{
			return eye;
		}

		inline const glm::vec3 getHeading() const
		{
			return heading;
		}

		inline const glm::mat4 getViewMatrix() const
		{
			return viewMatrix;
		}

		inline const glm::mat4 getProjectionMatrix() const
		{
			return projMatrix;
		}

	private:
		glm::vec3 eye = glm::vec3(10.0f, 10.0f, 10.0f); //Starting pos
		glm::vec3 heading = -eye; //This vector is always normalized
		float fov, aspect, near, far;
		const glm::vec3 UP = glm::vec3(0.0f, 1.0f, 0.0f);

		float pitch = 0.0f, yaw = 0.0f; //No roll since cam is First Person like (FPS)

		glm::mat4 viewMatrix = glm::mat4(1.0f);
		glm::mat4 projMatrix = glm::perspective(glm::radians(45.0f), 16.0f / 9.0f, 0.01f, 1000.0f);
	};

}