#include "loader.h"
#include "Log.h"
#include "Mesh.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#define STB_IMAGE_IMPLEMENTATION

#include <stb_image.h>

#include <glm\glm.hpp>
#include <iostream>
#include <vector>
#include <cstring>


std::string getSource(const std::string& sourceFile, const std::string& type)
{
	std::ifstream infile("shaders/" + sourceFile + "." + type + ".glsl");
	std::string source;
	std::stringstream stringStream;

	if (!infile.is_open())
	{
		FILE_LOG(logERROR) << "Couldn't open shader source: " + sourceFile;
		throw std::runtime_error("Couldn't open shader source: " + sourceFile);
	}

	stringStream << infile.rdbuf();
	source = stringStream.str();

	return source;
}

GLuint compileShader(const GLchar* source, GLenum type)
{
	GLuint shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, nullptr);
	glCompileShader(shader);

	GLint status;
	GLchar infolog[512];

	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (!status)
	{
		glGetShaderInfoLog(shader, 512, nullptr, infolog);
		FILE_LOG(logERROR) << "Compiling shader failed: " + std::string(infolog);
		throw std::runtime_error("Error compiling shader: " + std::string(infolog));
	}

	return shader;
}

GLuint createProgram(GLuint vertexShader, GLuint fragmentShader)
{
	GLuint program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);

	glLinkProgram(program);

	GLint status;
	GLchar infolog[512];

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status)
	{
		glGetProgramInfoLog(program, 512, nullptr, infolog);
		FILE_LOG(logERROR) << "Linking program failed: " + std::string(infolog);
		throw std::runtime_error("Error linking program: " + std::string(infolog));
	}

	return program;
}

GLuint createProgram(GLuint vertexShader, GLuint fragmentShader, GLuint geometryShader)
{
	GLuint program = glCreateProgram();

	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glAttachShader(program, geometryShader);

	glLinkProgram(program);

	GLint status;
	GLchar infolog[512];

	glGetProgramiv(program, GL_LINK_STATUS, &status);
	if (!status)
	{
		glGetProgramInfoLog(program, 512, nullptr, infolog);
		FILE_LOG(logERROR) << "Linking program failed: " + std::string(infolog);
		throw std::runtime_error("Error linking program: " + std::string(infolog));
	}

	return program;
}

bool foundIString(const char* str, const char* compare)
{
	if (std::string(str).find(compare) != std::string::npos)
	{
		//Found the substring
		return true;
	}
	return false;
}

namespace loader
{
	GLuint loadShader(const std::string& vertexShader, const std::string& fragmentShader)
	{
		std::string vSource, fSource;
		try
		{
			vSource = getSource(vertexShader, "vert");
			fSource = getSource(fragmentShader, "frag");
		}
		catch (std::runtime_error& e)
		{
			FILE_LOG(logERROR) << e.what();
			std::cout << e.what() << std::endl;
		}

		GLuint vsID, fsID;
		try
		{
			vsID = compileShader(vSource.c_str(), GL_VERTEX_SHADER);
			fsID = compileShader(fSource.c_str(), GL_FRAGMENT_SHADER);
		}
		catch (std::runtime_error& e)
		{
			FILE_LOG(logERROR) << e.what();
			std::cout << e.what() << std::endl;
			exit(-1);
		}
		GLuint programID;
		try
		{
			programID = createProgram(vsID, fsID);
		}
		catch (std::runtime_error& e)
		{
			FILE_LOG(logERROR) << e.what();
			std::cout << e.what() << std::endl;
		}

		glDeleteShader(vsID);
		glDeleteShader(fsID);

		return programID;
	}

	GLuint loadShader(const std::string & vertexShader, const std::string & fragmentShader, const std::string & geometryShader)
	{
		std::string vSource, fSource, gSource;
		try
		{
			vSource = getSource(vertexShader, "vert");
			fSource = getSource(fragmentShader, "frag");
			gSource = getSource(geometryShader, "geom");
		}
		catch (std::runtime_error& e)
		{
			FILE_LOG(logERROR) << e.what();
			std::cout << e.what() << std::endl;
		}

		GLuint vsID, fsID, gsID;
		try
		{
			vsID = compileShader(vSource.c_str(), GL_VERTEX_SHADER);
			fsID = compileShader(fSource.c_str(), GL_FRAGMENT_SHADER);
			gsID = compileShader(gSource.c_str(), GL_GEOMETRY_SHADER);
		}
		catch (std::runtime_error& e)
		{
			FILE_LOG(logERROR) << e.what();
			std::cout << e.what() << std::endl;
			exit(-1);
		}
		GLuint programID;
		try
		{
			programID = createProgram(vsID, fsID, gsID);
		}
		catch (std::runtime_error& e)
		{
			FILE_LOG(logERROR) << e.what();
			std::cout << e.what() << std::endl;
		}

		glDeleteShader(vsID);
		glDeleteShader(fsID);
		glDeleteShader(gsID);

		return programID;
	}

	ImageData * loadImageFromFile(const char * path, loader::ImageData::TAG datatype)
	{
		//REDO this (?)
		if (datatype == loader::ImageData::FLOAT)
		{
			if (foundIString(path, ".hdr"))
			{
				loader::ImageData* iData = new loader::ImageData;
				iData->tag = loader::ImageData::FLOAT;
				stbi_set_flip_vertically_on_load(true);
				int width, height, nrComponents;
				float *data = stbi_loadf(path, &width, &height, &nrComponents, 0);
				if (data)
				{
					iData->dtype.dataf = data;
					iData->height = height;
					iData->width = width;
					iData->nrComponents = nrComponents;
					FILE_LOG(logDEBUG) << "Successfully loaded image. Path: " + std::string(path);
					return iData;
				}
				FILE_LOG(logDEBUG) << "Could not load image. Path: " + std::string(path);
				return nullptr;
			}
			else if (foundIString(path, ".png") || foundIString(path, ".tga") || foundIString(path, ".jpg") || foundIString(path, ".jpeg"))
			{
				loader::ImageData* iData = new loader::ImageData;
				iData->tag = loader::ImageData::FLOAT;
				stbi_set_flip_vertically_on_load(true);
				int width, height, nrComponents;
				float *data = stbi_loadf(path, &width, &height, &nrComponents, 0);
				if (data)
				{
					iData->dtype.dataf = data;
					iData->height = height;
					iData->width = width;
					iData->nrComponents = nrComponents;
					FILE_LOG(logDEBUG) << "Successfully loaded image. Path: " + std::string(path);
					return iData;
				}
				FILE_LOG(logDEBUG) << "Could not load image. Path: " + std::string(path);
				return nullptr;
			}
			else
			{
				return nullptr;
			}
		}
		else if (datatype == loader::ImageData::BYTE)
		{
			if (foundIString(path, ".hdr"))
			{
				loader::ImageData* iData = new loader::ImageData;
				iData->tag = loader::ImageData::BYTE;
				stbi_set_flip_vertically_on_load(true);
				int width, height, nrComponents;
				unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
				if (data)
				{
					iData->dtype.datab = data;
					iData->height = height;
					iData->width = width;
					iData->nrComponents = nrComponents;
					FILE_LOG(logDEBUG) << "Successfully loaded image. Path: " + std::string(path);
					return iData;
				}
				FILE_LOG(logDEBUG) << "Could not load image. Path: " + std::string(path);
				return nullptr;
			}
			else if (foundIString(path, ".png") || foundIString(path, ".tga") || foundIString(path, ".jpg") || foundIString(path, ".jpeg"))
			{
				loader::ImageData* iData = new loader::ImageData;
				iData->tag = loader::ImageData::BYTE;
				//TODO: #error Change the vertical flip load to always false
				if(!foundIString(path, ".tga")) 
					stbi_set_flip_vertically_on_load(true);
				else
					stbi_set_flip_vertically_on_load(false);
				int width, height, nrComponents;
				unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
				if (data)
				{
					iData->dtype.datab = data;
					iData->height = height;
					iData->width = width;
					iData->nrComponents = nrComponents;
					FILE_LOG(logDEBUG) << "Successfully loaded image. Path: " + std::string(path);
					return iData;
				}
				FILE_LOG(logDEBUG) << "Could not load image. Path: " + std::string(path);
				return nullptr;
			}
			else
			{
				FILE_LOG(logDEBUG) << "Unrecognized image extension. Path: " + std::string(path);
				return nullptr;
			}
		}
		else
		{
			FILE_LOG(logDEBUG) << "Unrecognized image datatype. Path: " + std::string(path);
			return nullptr;
		}
	}


#pragma warning( push )
#pragma warning( disable : 4715 )
	std::vector<Mesh*> loadModelFromFile(const char* path)
	{
		if (foundIString(path, ".obj") || foundIString(path, ".fbx") || foundIString(path, ".dae")) //Found a OBJ/FBX/DAE file
		{
			const struct aiScene* scene = aiImportFile(path, aiProcess_Triangulate | aiProcess_JoinIdenticalVertices | aiProcess_GenSmoothNormals | aiProcess_CalcTangentSpace | aiProcess_SortByPType);// | aiProcess_FlipUVs);
			if (scene == nullptr)
			{
				FILE_LOG(logERROR) << "Attempted to load a mesh... Maybe filename is wrong?";
				return std::vector<Mesh*>();
			}
			if (!scene->HasMeshes())
			{
				FILE_LOG(logWARNING) << "Attempted to load an object with no meshes. Ignoring...";
				return std::vector<Mesh*>();
			}

			std::vector<Mesh*> loadedMeshes;

			for (unsigned int i = 0; i < scene->mNumMeshes; i++) //Load all meshes into a single file. Considering static meshes only
			{
				const aiMesh* mesh = scene->mMeshes[i];
				std::vector<glm::vec3> vertices;
				std::vector<unsigned int> indices;
				std::vector<glm::vec3> normals;
				std::vector<glm::vec3> tangents;
				std::vector<glm::vec3> bitangents;
				std::vector<glm::vec2> texcoords;

				if (mesh->HasPositions())
				{
					for (unsigned int j = 0; j < mesh->mNumVertices; j++)
					{
						glm::vec3 vertex;
						vertex.x = mesh->mVertices[j].x;
						vertex.y = mesh->mVertices[j].y;
						vertex.z = mesh->mVertices[j].z;
						vertices.push_back(vertex);
					}
				}
				if (mesh->HasFaces())
				{
					for (unsigned int j = 0; j < mesh->mNumFaces; j++)
					{
						for (unsigned int k = 0; k < mesh->mFaces[j].mNumIndices; k++)
						{
							indices.push_back(mesh->mFaces[j].mIndices[k]);
						}
					}
				}
				if (mesh->HasNormals())
				{
					for (unsigned int j = 0; j < mesh->mNumVertices; j++)
					{
						glm::vec3 normal;
						normal.x = mesh->mNormals[j].x;
						normal.y = mesh->mNormals[j].y;
						normal.z = mesh->mNormals[j].z;
						normals.push_back(normal);
					}
				}
				if (mesh->HasTangentsAndBitangents())
				{
					for (unsigned int j = 0; j < mesh->mNumVertices; j++)
					{
						glm::vec3 tangent, bitangent;
						tangent.x = mesh->mTangents[j].x;
						tangent.y = mesh->mTangents[j].y;
						tangent.z = mesh->mTangents[j].z;

						bitangent.x = mesh->mBitangents[j].x;
						bitangent.y = mesh->mBitangents[j].y;
						bitangent.z = mesh->mBitangents[j].z;

						tangents.push_back(tangent);
						bitangents.push_back(bitangent);
					}
				}
				if (mesh->HasTextureCoords(0))
				{
					for (unsigned int j = 0; j < mesh->mNumVertices; j++)
					{
						glm::vec2 texcoord;
						texcoord.x = mesh->mTextureCoords[0][j].x;
						texcoord.y = mesh->mTextureCoords[0][j].y;
						texcoords.push_back(texcoord);
					}
				}
				loadedMeshes.push_back(new Mesh(vertices, indices, normals, tangents, bitangents, texcoords));
			}

			FILE_LOG(logDEBUG) << "Successfully loaded model. Path: " + std::string(path);

			return loadedMeshes;
		}
	}
#pragma warning( pop ) 

	std::pair<std::vector<Mesh*>, unsigned int> loadMeshFromBIN(const std::string & path)
	{
		return std::pair<std::vector<Mesh*>, unsigned int>();
	}
}
