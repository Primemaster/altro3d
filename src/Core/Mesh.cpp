#include "Mesh.h"

Mesh::Mesh(std::vector<glm::vec3> vertices, std::vector<unsigned int> indices, std::vector<glm::vec3> normals, std::vector<glm::vec3> tangents, std::vector<glm::vec3> bitangents, std::vector<glm::vec2> uvs)
{
	this->vertices = vertices;
	this->indices = indices;
	this->normals = normals;
	this->tangents = tangents;
	this->bitangents = bitangents;
	this->uvs = uvs;
}

Mesh::Mesh(std::vector<glm::vec3> vertices)
{
	this->vertices = vertices;

	indices.clear();
	indices.shrink_to_fit();

	normals.clear();
	normals.shrink_to_fit();

	tangents.clear();
	tangents.shrink_to_fit();

	bitangents.clear();
	bitangents.shrink_to_fit();

	uvs.clear();
	uvs.shrink_to_fit();
}
