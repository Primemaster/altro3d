#include "GameObject.h"
#include "Log.h"

#include "Component.h"
#include "Components/GraphicsComponent.h"
#include "Components/TransformComponent.h"
#include "Components/Light.h"

namespace Altro3D
{
	GameObject::GameObject()
	{
		FILE_LOG(logINFO) << "GameObject [" << uid << "] created.";
	}

	GameObject::~GameObject()
	{
	}

	void GameObject::addComponent(GraphicsComponent * component)
	{
			components.emplace(std::type_index(typeid(GraphicsComponent)), component);
			component->linkedGO = this;
	}

	void GameObject::addComponent(TransformComponent * component)
	{
			components.emplace(std::type_index(typeid(TransformComponent)), component);
			component->linkedGO = this;
	}

	void GameObject::addComponent(Light * component)
	{
		components.emplace(std::type_index(typeid(Light)), component);
	}

	void GameObject::deleteComponent(GraphicsComponent * component)
	{
		auto it = components.find(std::type_index(typeid(GraphicsComponent)));
		if (it == components.end())
			FILE_LOG(logWARNING) << "A GameObject [" << uid << "] was deleting a non existant component.";
		else
		{
			components.erase(it);
			component->linkedGO = nullptr;
		}
	}

	void GameObject::deleteComponent(TransformComponent * component)
	{
		auto it = components.find(std::type_index(typeid(TransformComponent)));
		if (it == components.end())
			FILE_LOG(logWARNING) << "A GameObject [" << uid << "] was deleting a non existant component.";
		else
		{
			components.erase(it);
			component->linkedGO = nullptr;
		}
	}
	void GameObject::deleteComponent(Light * component)
	{
		auto it = components.find(std::type_index(typeid(Light)));
		if (it == components.end())
			FILE_LOG(logWARNING) << "A GameObject [" << uid << "] was deleting a non existant component.";
		else
		{
			components.erase(it);
			component->linkedGO = nullptr;
		}
	}
}
