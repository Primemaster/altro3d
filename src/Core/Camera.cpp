#include "Camera.h"
#include <gl/glew.h>
#include <string>


namespace Altro3D
{
	Camera::Camera()
	{
		//Default cam values
		fov = PI / 4.0f;
		aspect = 16.0f / 9.0f;
		near = 0.01f;
		far = 1000.0f;

		configure();
	}

	void Camera::calculateHeadingRH(float pitchChange, float yawChange)
	{
		pitch += pitchChange;
		yaw += yawChange;

		//Regularize the values between 0 and PI or 0 and 2PI accordingly
		if (pitch >= PI - 0.005f) pitch = PI - 0.005f;
		else if (pitch <= 0.005f) pitch = 0.005f;
		if (yaw >= 2 * PI) yaw = 0;
		else if (yaw <= 0) yaw = 2 * PI;

		//Spherical to cartesian conversion
		heading = glm::vec3(glm::cos(yaw) * glm::sin(pitch), glm::cos(pitch), glm::sin(yaw) * glm::sin(pitch));
		//std::cout << "Pitch / Yaw / ChangePitch: " << "[" << pitch << "] [" << yaw << "] [" << pitchChange << "]" << std::endl;
		//std::cout << "Heading: " << "[" << heading.x << "] [" << heading.y << "] [" << heading.z << "]" << std::endl;
		viewMatrix = glm::lookAt(eye, eye + heading, UP);
	}

	void Camera::setEye(glm::vec3 position)
	{
		eye = position;
	}

	void Camera::configure()
	{
		projMatrix = glm::perspective(fov, aspect, near, far);
	}
}