#pragma once
#include "../AL3D_EXP.h"

namespace Altro3D
{
	class GameObject;
}

class AL3D_API Component
{
public:
	Altro3D::GameObject* linkedGO;

	virtual ~Component() {  };
};

