#pragma once
#include <GL\glew.h>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <exception>
#include <stb_image.h>

class Mesh;

namespace loader
{
	struct ImageData
	{
		enum TAG { FLOAT, BYTE } tag;
		union DATATYPE
		{
			//Choose between two types of data
			float* dataf;
			unsigned char* datab;
		}dtype;

		int width;
		int height;
		int nrComponents;
		~ImageData()
		{
			switch (tag)
			{
			case loader::ImageData::FLOAT:
				stbi_image_free(dtype.dataf);
				break;
			case loader::ImageData::BYTE:
				stbi_image_free(dtype.datab);
				break;
			}
		};
	};
}

namespace loader
{
	GLuint loadShader(const std::string& vertexShader, const std::string& fragmentShader);
	GLuint loadShader(const std::string& vertexShader, const std::string& fragmentShader, const std::string& geometryShader);

	std::vector<Mesh*> loadModelFromFile(const char* path);
	std::pair<std::vector<Mesh*>, unsigned int> loadMeshFromBIN(const std::string& path); //TODO: Create loadMeshFromBin -> including preferred format

	ImageData* loadImageFromFile(const char* path, ImageData::TAG datatype);
}
