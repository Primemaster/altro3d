#pragma once
#include "Renderer.h"
#include "../Programs/SkyShader.h"

namespace Altro3D
{
	class Camera;

	class AL3D_API SkyRenderer : public Renderer
	{
	public:
		SkyRenderer();
		~SkyRenderer();

		void update(Altro3D::Camera& cam) override;
		void draw() override;

	private:
		SkyShader shader;
	};
}

