#pragma once
#include "Renderer.h"
#include "../Components/Light.h"
#include "../Programs/SMShader.h"
#include "../Programs/SM_CBShader.h"

constexpr unsigned int SHADOW_RES = 2048U;

namespace Altro3D
{
	class ShadowRenderer : public Renderer
	{
	public:
		friend class FSRenderer;
		ShadowRenderer();
		~ShadowRenderer();

		void update(Altro3D::Camera& cam) override;
		void draw() override;

		void init(std::vector<Altro3D::Light*> * lights);

		void calculateDepthMaps(std::vector<Altro3D::GraphicsComponent*> * components);
		glm::mat4 getLightSpaceMatrix(Altro3D::Light* key);

	private:
		std::map<Altro3D::Light*, GLuint> directionalLightMaps;
		std::map<Altro3D::Light*, GLuint> omniLightMaps;

		GLuint FBO;

		const glm::vec3 UP = glm::vec3(0.0f, 1.0f, 0.0f);
		const float NEAR_PLANE = 0.1f, FAR_PLANE = 100.0f;

		SMShader shader;
		SM_CBShader shader_CB;
	};
}
