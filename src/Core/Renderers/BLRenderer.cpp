#include "BLRenderer.h"

namespace Altro3D
{
	BLRenderer::BLRenderer()
	{
		shader.bind();
		shader.setTextureUnit("billboard", 0);
		shader.unbind();
	}

	BLRenderer::~BLRenderer()
	{
	}

	void BLRenderer::update(Altro3D::Camera & cam)
	{
		//Blank
	}

	void BLRenderer::draw()
	{
		shader.bind();
		glActiveTexture(GL_TEXTURE0);
		//glDisable(GL_CULL_FACE);

		for (auto gc : GComponentsList)
		{
			glm::vec2 center = glm::vec2(gc->linkedGO->getComponent<Altro3D::TransformComponent>()->getTranslation());
			glm::vec2 size = glm::vec2(gc->linkedGO->getComponent<Altro3D::TransformComponent>()->getScale());
			shader.setCenter(center);
			shader.setSize(size);

			gc->bindTextureObject(GraphicsComponent::tSlot::TEX0, GL_TEXTURE_2D);
			gc->bindVertexArrayObject(0);
			glDrawElements(GL_TRIANGLES, gc->getIndexSize(0), GL_UNSIGNED_INT, nullptr);
		}

		glBindVertexArray(0);

		//glEnable(GL_CULL_FACE);
		shader.unbind();
	}
}
