#include "FSRenderer.h"
#include "../Camera.h"

#include "../Components/GraphicsComponent.h"
#include "../Components/TransformComponent.h"
#include "../../App/ResourceManager.h"

#include "../GameObject.h"

namespace Altro3D
{
	FSRenderer::FSRenderer() : srenderer()
	{
		shader.bind();

		shader.setTextureUnit("albedoMap", 0);
		shader.setTextureUnit("normalMap", 1);
		shader.setTextureUnit("roughnessMap", 2);
		shader.setTextureUnit("metalicMap", 3);
		shader.setTextureUnit("emissiveMap", 4);
		shader.setTextureUnit("skybox", 5);

		int values[8];

		for (int i = 0; i < 8; i++)
		{
			values[i] = 6 + i;
		}

		shader.setTextureUnitArray("shadowMap", 8, values);

		for (int i = 0; i < 8; i++)
		{
			values[i] = 14 + i;
		}

		shader.setTextureUnitArray("shadowMapCube", 8, values);

		shader.unbind();

		GameObject* go = new GameObject();
		TransformComponent* tc = new TransformComponent();

		go->addComponent(gc);
		go->addComponent(tc);
		
		blrenderer.addGComponent(gc);
	}


	FSRenderer::~FSRenderer()
	{
	}

	void FSRenderer::update(Altro3D::Camera& cam)
	{

		static bool a = true;
		if (a)
		{
			smrenderer.init(&LComponentsList);
			a = false;
		}
		srenderer.update(cam);

		shader.bind();
		shader.setProjViewMatrix(cam.getProjectionMatrix() * cam.getViewMatrix());
		shader.setCameraPosition(cam.getEye());

		Altro3D::Light::loadInShader(&LComponentsList);
		shader.unbind();

		blrenderer.update(cam);
	}

	void FSRenderer::draw()
	{
		srenderer.draw();

		smrenderer.calculateDepthMaps(&GComponentsList);

		shader.bind();

		static glm::mat4 dlms[8];

		auto dlmSize = smrenderer.directionalLightMaps.size();
		auto olmSize = smrenderer.omniLightMaps.size();

		for (auto i = 0; i < dlmSize; i++)
		{
			uint unit = 0;
			Light* l = LComponentsList[i];
			if (!l->isDirectional())
			{
				continue;
			}
			auto val = smrenderer.directionalLightMaps[l];
			gc->addTexture2D_GLInternal(val, GraphicsComponent::tSlot::TEX0);
			auto lsMatrix = smrenderer.getLightSpaceMatrix(l);
			glActiveTexture(GL_TEXTURE6 + unit++);
			glBindTexture(GL_TEXTURE_2D, val);
			dlms[i] = lsMatrix;
		}

		for (auto i = 0; i < olmSize; i++)
		{
			uint unit = 0;
			Light* l = LComponentsList[i];
			if (l->isDirectional())
			{
				continue;
			}
			auto val = smrenderer.omniLightMaps[l];
			glActiveTexture(GL_TEXTURE14 + unit++);
			glBindTexture(GL_TEXTURE_CUBE_MAP, val);
		}

		if(dlmSize != 0)
			shader.setLightSpaceMatrixArray(dlms, dlmSize);

		if(olmSize != 0)
			shader.setFarPlane(smrenderer.FAR_PLANE);

		if (skybox != nullptr)
		{
			glActiveTexture(GL_TEXTURE5);
			skybox->bindTextureObject(GraphicsComponent::TEX0, GL_TEXTURE_CUBE_MAP);
		}

		for (auto gc : GComponentsList)
		{
			shader.setModelMatrix(gc->linkedGO->getComponent<Altro3D::TransformComponent>()->getModelMatrix());
			//TODO: Pluck this into a method
			if (gc->rendertype == GraphicsComponent::rType::RENDER_DEFAULT)
			{
				shader.setRenderType(static_cast<unsigned int>(GraphicsComponent::rType::RENDER_DEFAULT));
				for (uint i = 0; i < 5; i++)
				{
					glActiveTexture(GL_TEXTURE0 + i);
					gc->bindTextureObject(static_cast<GraphicsComponent::tSlot>(i), GL_TEXTURE_2D);
				}
			}
			else if(gc->rendertype == GraphicsComponent::rType::RENDER_COLORFILL_ONLY)
			{
				shader.setRenderType(static_cast<unsigned int>(GraphicsComponent::rType::RENDER_COLORFILL_ONLY));
				shader.setModelColorFill(gc->getModelFillColor());
			}

			for (unsigned int i = 0; i < gc->getDrawSize(); i++)
			{
				gc->bindVertexArrayObject(i);
				glDrawElements(GL_TRIANGLES, gc->getIndexSize(i), GL_UNSIGNED_INT, nullptr);
			}
		}
		/*glBindTexture(GL_TEXTURE_2D, 0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);*/
		glBindVertexArray(0);

		shader.unbind();

		blrenderer.draw();
	}

	void FSRenderer::addSkyBox(Altro3D::GraphicsComponent * skygc)
	{
		srenderer.addGComponent(skygc);
		skybox = skygc;
	}

	void FSRenderer::addBillboard(Altro3D::GraphicsComponent * billboard)
	{
		blrenderer.addGComponent(billboard);
	}
}

