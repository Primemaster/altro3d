#include "Renderer.h"
#include "../Log.h"
#include <algorithm>
#include <cctype>
#include <cstdint>

namespace Altro3D
{
	unsigned int Renderer::getPriority() const
	{
		return priority;
	}

	void Renderer::addGComponent(Altro3D::GraphicsComponent * graphicsComponent)
	{
		GComponentsList.push_back(graphicsComponent);
	}

	void Renderer::delGComponent(Altro3D::GraphicsComponent * graphicsComponent)
	{
		if (std::find(GComponentsList.begin(), GComponentsList.end(), graphicsComponent) == GComponentsList.end())
			FILE_LOG(logWARNING) << "Renderer attempted to delete a non existant component. ID [" << TO_UINTPTR_T(graphicsComponent) << "]";
		else
			GComponentsList.erase(std::remove(GComponentsList.begin(), GComponentsList.end(), graphicsComponent), GComponentsList.end());
	}

	void Renderer::addLComponent(Altro3D::Light * light)
	{
		LComponentsList.push_back(light);
	}

	void Renderer::delLComponent(Altro3D::Light * light)
	{
		if (std::find(LComponentsList.begin(), LComponentsList.end(), light) == LComponentsList.end())
			FILE_LOG(logWARNING) << "Renderer attempted to delete a non existant component. ID [" << TO_UINTPTR_T(light) << "]";
		else
			LComponentsList.erase(std::remove(LComponentsList.begin(), LComponentsList.end(), light), LComponentsList.end());
	}
}
