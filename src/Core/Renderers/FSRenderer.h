#pragma once
#include "Renderer.h"
#include "../Programs/FSShader.h"
#include "../Renderers/SkyRenderer.h"
#include "../Renderers/ShadowRenderer.h"
#include "../Renderers/BLRenderer.h"

namespace Altro3D
{
	class Camera;

	class AL3D_API FSRenderer : public Renderer
	{
	public:
		FSRenderer();
		~FSRenderer();

		void update(Altro3D::Camera& cam) override;
		void draw() override;

		void addSkyBox(Altro3D::GraphicsComponent* skygc);
		void addBillboard(Altro3D::GraphicsComponent* billboard);

	private:
		FSShader shader;
		SkyRenderer srenderer;
		ShadowRenderer smrenderer;
		BLRenderer blrenderer;

		GraphicsComponent* skybox = nullptr;

		GraphicsComponent* gc = new GraphicsComponent(GraphicsComponent::RENDER_BILLBOARD);
	};
}

