#include "ShadowRenderer.h"

#include "../Components/GraphicsComponent.h"
#include "../Components/TransformComponent.h"

#include <glm/gtc/matrix_transform.hpp>

#include "../GameObject.h"

namespace Altro3D
{
	ShadowRenderer::ShadowRenderer()
	{
	}

	ShadowRenderer::~ShadowRenderer()
	{
		glDeleteFramebuffers(1, &FBO);

		for (auto lm : directionalLightMaps)
		{
			glDeleteTextures(1, &lm.second);
		}
//#warning slightly different
		for (auto lm : omniLightMaps)
		{
			glDeleteTextures(1, &lm.second);
		}
	}

	void ShadowRenderer::update(Altro3D::Camera & cam)
	{
	}

	void ShadowRenderer::draw()
	{
	}

	void ShadowRenderer::init(std::vector<Altro3D::Light*>* lights)
	{
		glGenFramebuffers(1, &FBO);
		glBindFramebuffer(GL_FRAMEBUFFER, FBO);

		glDrawBuffer(GL_NONE);
		glReadBuffer(GL_NONE);

		LComponentsList = *lights;
		for (auto l : LComponentsList)
		{
			GLuint depthMap;
			glGenTextures(1, &depthMap);
			if (l->isDirectional())
			{
				glBindTexture(GL_TEXTURE_2D, depthMap);

				glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOW_RES, SHADOW_RES, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
				GLfloat borderColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
				glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
				
				directionalLightMaps.emplace(l, depthMap);
			}		
			else
			{
				glBindTexture(GL_TEXTURE_CUBE_MAP, depthMap);

				for (int i = 0; i < 6; i++)
				{
					glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT, SHADOW_RES, SHADOW_RES, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
				}

				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
				glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);	

				omniLightMaps.emplace(l, depthMap);
			}
//#warning redo on GS
		}
		FILE_LOG(logDEBUG) << "Loading " << LComponentsList.size() << " lights at runtime!";
		FILE_LOG(logDEBUG) << "ShadowMap resolution: " << SHADOW_RES << " X " << SHADOW_RES << " px.";
		FILE_LOG(logDEBUG) << "Using PCF with 16 samples...";

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

	void ShadowRenderer::calculateDepthMaps(std::vector<Altro3D::GraphicsComponent*>* components)
	{
		glViewport(0, 0, SHADOW_RES, SHADOW_RES);
		glBindFramebuffer(GL_FRAMEBUFFER, FBO);
		//TODO: Make this variable in the future, also for point lights
		glm::mat4 lightProjection = glm::ortho(-50.0f, 50.0f, -50.0f, 50.0f, NEAR_PLANE, FAR_PLANE);

		shader.bind();
		glDisable(GL_CULL_FACE);
		for (auto lm : directionalLightMaps)
		{
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, lm.second, 0);
			//Set all the view matrices for the the lights
			//TODO: Add methods for lightmaps at the camera position
			glm::mat4 lightView = glm::lookAt(glm::vec3(lm.first->properties.lightPosition), glm::vec3(0.0f), UP);
			glm::mat4 lightSpace = lightProjection * lightView;

			shader.setLightSpaceMatrix(lightSpace);
			glClear(GL_DEPTH_BUFFER_BIT);
			for (auto gc : *components)
			{
				shader.setModelMatrix(gc->linkedGO->getComponent<Altro3D::TransformComponent>()->getModelMatrix());

				for (unsigned int i = 0; i < gc->getDrawSize(); i++)
				{
					gc->bindVertexArrayObject(i);
					glDrawElements(GL_TRIANGLES, gc->getIndexSize(i), GL_UNSIGNED_INT, nullptr);
				}
			}
			glBindVertexArray(0);
		}
		shader.unbind();
		
		shader_CB.bind();

		static const float aspect = 1.0f;

		lightProjection = glm::perspective<float>(glm::radians(90.0f), aspect, NEAR_PLANE, FAR_PLANE);

		for (auto lm : omniLightMaps)
		{
			//Set all the view matrices for the the lights
			//TODO: Add methods for lightmaps at the camera position
			const glm::vec3 lp = glm::vec3(lm.first->properties.lightPosition);

			glm::mat4 lightView[6] = { 
				glm::lookAt(lp, lp + glm::vec3(1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)),
				glm::lookAt(lp, lp + glm::vec3(-1.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f, 0.0f)),
				glm::lookAt(lp, lp + glm::vec3(0.0f, 1.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)),
				glm::lookAt(lp, lp + glm::vec3(0.0f, -1.0f, 0.0f), glm::vec3(0.0f, 0.0f, -1.0f)),
				glm::lookAt(lp, lp + glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, -1.0f)),
				glm::lookAt(lp, lp + glm::vec3(0.0f, 0.0f, -1.0f), glm::vec3(0.0f, 0.0f, -1.0f)) 
			};

			glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, lm.second, 0);

			glm::mat4 lightSpace[6] = {
				 lightProjection * lightView[0],
				 lightProjection * lightView[1],
				 lightProjection * lightView[2],
				 lightProjection * lightView[3],
				 lightProjection * lightView[4],
				 lightProjection * lightView[5]
			};

			shader_CB.setLightSpaceMatrixArray(lightSpace);
			
			shader_CB.setLightPos(lp);
			shader_CB.setFarPlane(FAR_PLANE);

			glClear(GL_DEPTH_BUFFER_BIT);
			for (auto gc : *components)
			{
				shader_CB.setModelMatrix(gc->linkedGO->getComponent<Altro3D::TransformComponent>()->getModelMatrix());

				for (unsigned int i = 0; i < gc->getDrawSize(); i++)
				{
					gc->bindVertexArrayObject(i);
					glDrawElements(GL_TRIANGLES, gc->getIndexSize(i), GL_UNSIGNED_INT, nullptr);
				}
			}
			glBindVertexArray(0);
		}
		glEnable(GL_CULL_FACE);
		shader_CB.unbind();

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
//#warning remove the hardcoded Viewport
		glViewport(0, 0, 1280, 720);

		/*GLenum error = glGetError();*/

		/*FILE_LOG(logERROR) << "ERROR 0x0" << std::hex << error;*/
	}

	glm::mat4 ShadowRenderer::getLightSpaceMatrix(Altro3D::Light * key)
	{
		glm::mat4 lightProjection = glm::ortho(-50.0f, 50.0f, -50.0f, 50.0f, NEAR_PLANE, FAR_PLANE);
		glm::mat4 lightView = glm::lookAt(glm::vec3(key->properties.lightPosition), glm::vec3(0.0f), UP);
		glm::mat4 lightSpace = lightProjection * lightView;

		return lightSpace;
	}
}