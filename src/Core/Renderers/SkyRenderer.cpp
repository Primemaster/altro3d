#include "SkyRenderer.h"
#include "../Camera.h"
#include "../../Core/Mesh.h"
#include "../Components/GraphicsComponent.h"

namespace Altro3D
{
	SkyRenderer::SkyRenderer()
	{
		shader.bind();

		shader.setTextureUnit("environmentMap", 0);

		shader.unbind();
	}


	SkyRenderer::~SkyRenderer()
	{
	}

	void SkyRenderer::update(Altro3D::Camera & cam)
	{
		shader.bind();
		shader.setProjMatrix(cam.getProjectionMatrix());
		shader.setViewMatrix(cam.getViewMatrix());
		shader.unbind();
	}

	void SkyRenderer::draw()
	{
		static float skyboxVertices[] = {
			//positions          
			-1.0f,  1.0f, -1.0f,
			-1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f, -1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,

			-1.0f, -1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f, -1.0f,  1.0f,
			-1.0f, -1.0f,  1.0f,

			-1.0f,  1.0f, -1.0f,
			 1.0f,  1.0f, -1.0f,
			 1.0f,  1.0f,  1.0f,
			 1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f,  1.0f,
			-1.0f,  1.0f, -1.0f,

			-1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			 1.0f, -1.0f, -1.0f,
			 1.0f, -1.0f, -1.0f,
			-1.0f, -1.0f,  1.0f,
			 1.0f, -1.0f,  1.0f
		};

		static GLuint vao = 0;
		static GLuint vbo;
		if (vao == 0)
		{
			shader.bind();
			glGenVertexArrays(1, &vao);
			glGenBuffers(1, &vbo);

			glBindVertexArray(vao); //PV Atributes next

			glBindBuffer(GL_ARRAY_BUFFER, vbo);
			glBufferData(GL_ARRAY_BUFFER, 3 * 36 * sizeof(float), &skyboxVertices, GL_STATIC_DRAW);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid*)0);
			glEnableVertexAttribArray(0);

			glBindBuffer(GL_ARRAY_BUFFER, 0);
			glBindVertexArray(0);
			shader.unbind();
		}

		if (!GComponentsList.empty())
		{
			shader.bind();
			glDepthFunc(GL_LEQUAL);
			glActiveTexture(GL_TEXTURE0);
			GComponentsList[0]->bindTextureObject(GraphicsComponent::TEX0, GL_TEXTURE_CUBE_MAP);

			glDepthMask(GL_FALSE);
			glBindVertexArray(vao);
			glDrawArrays(GL_TRIANGLES, 0, 36);
			glDepthMask(GL_TRUE);
			glDepthFunc(GL_LESS);

			glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			glBindVertexArray(0);
			shader.unbind();
		}
		
	}
}