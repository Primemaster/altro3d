#pragma once
#include "Renderer.h"
#include "../Programs/BLShader.h"
#include "../GameObject.h"
#include "../Components/TransformComponent.h"

namespace Altro3D
{
	class BLRenderer : public Renderer
	{
	public:
		BLRenderer();
		~BLRenderer();

		void update(Altro3D::Camera& cam) override;
		void draw() override;

	private:
		BLShader shader;
	};
}

