#pragma once
#include <vector>

#include "../Components/GraphicsComponent.h"
#include "../Components/Light.h"

namespace Altro3D
{
	class Camera;
	//NOTE: Deleting a Renderer does not delete it's previously assigned (Graphics)Components whatsoever
	class AL3D_API Renderer
	{
	public:
		Renderer() = default;
		~Renderer() = default;

		virtual void update(Altro3D::Camera& cam) = 0;
		virtual void draw() = 0;

		unsigned int getPriority() const;

		//Default functions
		void addGComponent(Altro3D::GraphicsComponent* graphicsComponent);
		void delGComponent(Altro3D::GraphicsComponent* graphicsComponent);

		void addLComponent(Altro3D::Light* light);
		void delLComponent(Altro3D::Light* light);

	protected:
		std::vector<Altro3D::GraphicsComponent*> GComponentsList;
		std::vector<Altro3D::Light*> LComponentsList;

		//This is protected so we are able to change it in a child class to prioritize in pipeline acording its function
		unsigned int priority = 0;
	};
}
