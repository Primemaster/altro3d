#pragma once
#include "../AL3D_EXP.h"
#include "../Core/loader.h"
#include "../Core/Log.h"
#include "../Core/Mesh.h"

#define TO_UINTPTR_T(A) reinterpret_cast<std::uintptr_t>(A)

#include <map>
#include <vector>

namespace Altro3D
{
	class GraphicsComponent;

#pragma warning( push )
#pragma warning( disable : 4251 )
	class AL3D_API ResourceManager
	{
	public:
		friend class Altro3D::GraphicsComponent;

		enum ImageFormat
		{
			DEFAULT
		};

		ResourceManager() = default;
		~ResourceManager();

		void deleteAll();

		void addModelFromFile(const char* modelName, const char* filepath);
		unsigned int storedModelsCount() const;

		void addImageFromFile(const char* imageName, const char* filepath, ImageFormat imformat);
		unsigned int storedImagesCount() const;

	private:
		std::map<const char*, std::vector<Mesh*>> models;
		std::map<const char*, loader::ImageData*> images;
	};
#pragma warning( pop )
}
