#include "Scene.h"
#include "StateManager.h"

namespace Altro3D
{
	Scene::Scene() { }

	void Scene::changeScene(StateManager * sm, Scene * scene)
	{
		sm->changeState(scene);
	}
}
