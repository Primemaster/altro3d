#include "StateManager.h"
#include <iostream>
#include "../Core/Log.h"
#include "Scene.h"
#include "../Core/GameObject.h"

namespace Altro3D
{
	void StateManager::initialize(uint width, uint height, const char * title)
	{
		Output2FILE::Stream() = fopen("CV-log.log", "w");
		if (!glfwInit())
		{
			FILE_LOG(logERROR) << "Glfw3 failed to initialize...";
		}

		FILE_LOG(logDEBUG) << "Glfw3 initialized successfully...";

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
		glfwWindowHint(GLFW_SAMPLES, 4);

		window = glfwCreateWindow(width, height, title, /*glfwGetPrimaryMonitor()*/ NULL, NULL);
		FILE_LOG(logDEBUG) << "Window Created Successfully.";
		w = width;
		h = height;

		glfwMakeContextCurrent(window);


		glewExperimental = true;
		GLenum status = glewInit();
		if (status != GLEW_OK)
		{
			FILE_LOG(logERROR) << "Glew failed to initialize - Cause: " << glewGetErrorString(status);
		}
		else
		{
			FILE_LOG(logDEBUG) << "Glew initialized sucessfully - VERSION " << glewGetString(GLEW_VERSION);
		}

		glDepthFunc(GL_LESS);
		//glEnable(GL_FRAMEBUFFER_SRGB); Gama correction set to manual
		glEnable(GL_MULTISAMPLE);
		glEnable(GL_DEPTH_TEST);

		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		isRunning = true;
	}

	StateManager::StateManager(uint width, uint height, const char* windowName)
	{
		initialize(width, height, windowName);
	}

	void StateManager::cleanup()
	{
		//CleanUp states
		while (!gameScenes.empty())
		{
			gameScenes.top()->cleanup();
			gameScenes.pop();
		}

		glfwDestroyWindow(window);
		glfwTerminate();
		isRunning = false;
	}

	void StateManager::changeState(Scene * state)
	{
		if (!gameScenes.empty())
		{
			gameScenes.top()->cleanup();
			gameScenes.pop();
		}

		gameScenes.push(state);
		gameScenes.top()->initialize(this);
	}

	void StateManager::pushState(Scene * state)
	{
		if (!gameScenes.empty())
		{
			gameScenes.top()->pause();
		}

		gameScenes.push(state);
		gameScenes.top()->initialize(this);
	}

	void StateManager::popState()
	{
		if (!gameScenes.empty())
		{
			gameScenes.top()->cleanup();
			gameScenes.pop();
		}

		if (!gameScenes.empty())
		{
			gameScenes.top()->resume();
		}
	}

	void StateManager::handleEvents()
	{
		gameScenes.top()->handleEvents(this);

		//Handle exiting the window
		glfwPollEvents();
	}

	void StateManager::update()
	{
		gameScenes.top()->update(this);
	}

	void StateManager::draw()
	{
		gameScenes.top()->draw(this);

		glfwSwapBuffers(window);
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	bool StateManager::running()
	{
		return isRunning;
	}

	void StateManager::quit()
	{
		FILE_LOG(logINFO) << "Application terminating...";
		isRunning = false;
	}

}