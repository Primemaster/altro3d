#pragma once
#include "../AL3D_EXP.h"

namespace Altro3D
{
	class StateManager;

	class AL3D_API Scene
	{
	public:
		virtual void initialize(StateManager * sm) = 0;
		virtual void cleanup() = 0;

		virtual void pause() = 0;
		virtual void resume() = 0;

		virtual void handleEvents(StateManager * sm) = 0;
		virtual void update(StateManager * sm) = 0;
		virtual void draw(StateManager * sm) = 0;

		void changeScene(StateManager * sm, Scene * state);

	protected:
		Scene();
	};

}
