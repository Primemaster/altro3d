#pragma once

#include "../AL3D_EXP.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <stack>

namespace Altro3D
{
	typedef unsigned int uint;
	class Scene;

	class AL3D_API StateManager
	{
	public:
		void initialize(uint width, uint height, const char* title);

		GLFWwindow * Window()
		{
			return window;
		}
		const glm::dvec2 getWindowStats()
		{
			return glm::dvec2(w, h);
		}

		void cleanup();

		void pushState(Scene * state);
		void changeState(Scene * state);
		void popState();

		void handleEvents();
		void update();
		void draw();

		bool running();
		void quit();

		StateManager(uint width, uint height, const char* windowName);
	private:
		GLFWwindow* window;

		std::stack<Scene*> gameScenes;

		bool isRunning;
		uint w, h;
	};
}

