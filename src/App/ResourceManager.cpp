#include "ResourceManager.h"

namespace Altro3D
{
	ResourceManager::~ResourceManager()
	{
		for (std::map<const char*, std::vector<Mesh*>>::iterator it = models.begin(); it != models.end(); it++)
		{
			for (std::vector<Mesh*>::iterator itr = it->second.begin(); itr != it->second.end(); ++itr)
			{
				delete (*itr);
			}
			it->second.clear();
			it->second.shrink_to_fit();
		}
		models.clear();

		for (std::map<const char*, loader::ImageData*>::iterator it = images.begin(); it != images.end(); it++)
		{
			delete it->second;
		}
		images.clear();
	}

	void ResourceManager::deleteAll()
	{
		for (std::map<const char*, std::vector<Mesh*>>::iterator it = models.begin(); it != models.end(); it++)
		{
			for (std::vector<Mesh*>::iterator itr = it->second.begin(); itr != it->second.end(); ++itr)
			{
				delete (*itr);
			}
			it->second.clear();
			it->second.shrink_to_fit();
		}
		models.clear();

		for (std::map<const char*, loader::ImageData*>::iterator it = images.begin(); it != images.end(); it++)
		{
			delete it->second;
		}
		images.clear();
	}

	void ResourceManager::addModelFromFile(const char* modelName, const char* filepath)
	{
		std::vector<Mesh*> meshBatch = loader::loadModelFromFile(filepath);

		models.emplace(modelName, meshBatch);
	}
	unsigned int ResourceManager::storedModelsCount() const
	{
		return models.size();
	}

	void ResourceManager::addImageFromFile(const char * imageName, const char * filepath, ImageFormat imformat)
	{
		loader::ImageData* image = nullptr;

		switch (imformat)
		{
			case Altro3D::ResourceManager::DEFAULT:
				image = loader::loadImageFromFile(filepath, loader::ImageData::BYTE);
				break;
			default:
				break;
		}

		images[imageName] = image;
	}

	unsigned int ResourceManager::storedImagesCount() const
	{
		return images.size();
	}
}


