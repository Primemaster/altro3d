#pragma once
#include "../AL3D_EXP.h"

#include <GL\glew.h>
#include <GLFW\glfw3.h>


namespace Altro3D
{
	class AL3D_API Interface
	{
	public:
		static void getCursorPos(GLFWwindow* window, double* x, double* y);
		static void setCursorPos(GLFWwindow* window, double x, double y);
		static int  getKey(GLFWwindow* window, int key);

	};
}
