#include "interface.h"


namespace Altro3D
{
	void Interface::getCursorPos(GLFWwindow * window, double * x, double * y)
	{
		glfwGetCursorPos(window, x, y);
	}
	void Interface::setCursorPos(GLFWwindow * window, double x, double y)
	{
		glfwSetCursorPos(window, x, y);
	}
	int Interface::getKey(GLFWwindow * window, int key)
	{
		int rkey = glfwGetKey(window, key);
		return rkey;
	}
}
